import { ILeadFromDB } from "@/interfaces/lead-from-DB.interface";

export class Utils {

  static deleteQuatation(lead: ILeadFromDB) {
    lead.responsible_user_id_amo = lead.responsible_user_id_amo.substring(
      1,
      lead.responsible_user_id_amo.length - 1
    );
  }

  static changeStatusToTags(lead: ILeadFromDB) {
    lead["tags"] = [lead["status_id_amo"]];
    delete lead["status_id_amo"];
  }

  static convertDate(unix_timestamp: string | number | Date) {
    {
      const date: Date = new Date(unix_timestamp);
      return date
        .toLocaleString("ru-RU", {
          day: "numeric",
          month: "long",
          year: "numeric",
        })
        .slice(0, -3);
    }
  }

  static convertStatuses(lead: { status_id_amo?: string | number }): void {
    if (lead.status_id_amo === 52720942) {
      lead.status_id_amo = "Первичный контакт";
    }
    if (lead.status_id_amo === 52720945) {
      lead.status_id_amo = "Переговоры";
    }
    if (lead.status_id_amo === 52720948) {
      lead.status_id_amo = "Принимают решения";
    }
    if (lead.status_id_amo === 52720951) {
      lead.status_id_amo = "Согласование договора";
    }
  }

  static separatePriceWithSpace(price: string | number): string {
    const num = "" + price;
    return num.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1 ");
  }
}
