import { createApp } from 'vue'
import App from './AppCompApi.vue'

import {Button, Checkbox, Row, Divider, Col, Table, Tag, Avatar, Space, Spin, Input} from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

const app = createApp(App)


app.use(Button)
app.use(Checkbox)
app.use(Divider)
app.use(Row)
app.use(Col)
app.use(Table)
app.use(Tag)
app.use(Avatar)
app.use(Space)
app.use(Spin)
app.use(Input)
app.mount('#app')

