import { ILeadFromDB } from "@/interfaces/lead-from-DB.interface";
import { Utils } from "@/utils/utils";
import axios from "axios";
import { Ref } from "vue";

// TODO any ?
export default function useForm(
  isPostLoading: Ref<boolean>,
  data: Ref<object[]>,
  fetchingLeads: () => void
) {
  const searching = async (stringFromInput: string): Promise<void> => {
    if (stringFromInput.length >= 3) {
      try {
        isPostLoading.value = true;
        const response = await axios({
          method: "get",
          url: `http://localhost:3000/?queryString=${stringFromInput}`,
          headers: {
            "Content-Type": "application/json",
          },
        });
        if (response.data) {
          const leadsWithContactsRelationsDB: ILeadFromDB[] =
            response.data.answer;

          for (const lead of leadsWithContactsRelationsDB) {
            Utils.deleteQuatation(lead);

            lead.createdAt = Utils.convertDate(lead.createdAt);
            lead.price = Utils.separatePriceWithSpace(lead.price) + " ₽";

            Utils.convertStatuses(lead);
            Utils.changeStatusToTags(lead);
          }
          data.value = leadsWithContactsRelationsDB;
        }
      } catch (error) {
        console.log(error);
      } finally {
        isPostLoading.value = false;
      }
    }
    if (stringFromInput.length === 0) {
      fetchingLeads();
    }
  };
  return {
    searching,
  };
}
