import { ILeadFromDB } from "@/interfaces/lead-from-DB.interface";
import axios from "axios";
import { onMounted, Ref, ref } from "vue";
import {Utils} from '@/utils/utils'

export default function usePost() {
  const data: Ref<ILeadFromDB[]> = ref([]);
  const isPostLoading: Ref<boolean> = ref(true);

  const fetchingLeads = async function fetchingLeads(): Promise<void> {
    try {
      isPostLoading.value = true;

      const response = await axios.get("http://localhost:3000/");
      const leadsWithContactsRelationsDB: ILeadFromDB[] = response.data.answer;

      for (const lead of leadsWithContactsRelationsDB) {

        Utils.deleteQuatation(lead)

        lead.createdAt = Utils.convertDate(lead.createdAt);
        lead.price = Utils.separatePriceWithSpace(lead.price) + " ₽";

        Utils.convertStatuses(lead);
        Utils.changeStatusToTags(lead)
      }
      data.value = leadsWithContactsRelationsDB;
    } catch (error) {
      console.log(error);
    } finally {
      isPostLoading.value = false;
    }
  };

  onMounted(fetchingLeads);
  return {
    data,
    isPostLoading,
    fetchingLeads,
  };
}
