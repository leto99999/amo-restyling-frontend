export interface ILeadFromDB {
  key?: number;
  id?: number;
  lead_id_amo: number;
  name: string;
  price: number | string;
  status_id_amo?: any;
  // TODO any
  tags?: Array<string | number>;
  contacts_ids_amo: string;
  tags_amo: string;
  lead_updated_at_amo: string;
  responsible_user_id_amo: string;
  createdAt: string;
  updatedAt: string;
  contactsEntityIMNAP: [];
}
