const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    allowedHosts: "all"
  },
  css: {
    extract: false,
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true
        }
      }
    }
  },
  // devServer: {
  //   https: true,
  //   host: 'localhost',
  //   // port: 3000,
  //   // publicPath: `https://localhost:3000`,
  //   allowedHosts: 'all',
  //   headers: {
  //     'Access-Control-Allow-Origin': '*',
  //     'Access-Control-Allow-Headers': '*',
  //     'Access-Control-Allow-Methods': '*'
  //   }
  // }

})
